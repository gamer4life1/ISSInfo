#!/bin/python3
import json
import turtle
import urllib.request

url1 = 'http://api.open-notify.org/astros.json'
response = urllib.request.urlopen(url1)
result = json.loads(response.read())
print("People in space: ", result["number"])

people = result["people"]
for p in people:
  print(p["name"], "is in the", p["craft"])

url2 = 'http://api.open-notify.org/iss-now.json'
response = urllib.request.urlopen(url2)
result = json.loads(response.read())
location = result["iss_position"]
lat = location["latitude"]
lon = location["longitude"]
print("Latitude: ", lat)
print("Longitude: ", lon)

screen = turtle.Screen()
screen.setup(720, 360)
screen.setworldcoordinates(-180, -90, 180, 90)
screen.bgpic('map.jpeg')

screen.register_shape("iss.png")
iss = turtle.Turtle()
iss.shape("iss.png")
iss.setheading(90)

iss.penup()
iss.goto(lon, lat)
